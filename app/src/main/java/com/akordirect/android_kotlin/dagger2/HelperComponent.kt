package com.akordirect.android_kotlin.dagger2

import com.akordirect.android_kotlin.activities.MainActivity
import dagger.Component


@Component(modules = [StorageModule::class, InfoModule::class])
interface HelperComponent {

    fun injectMainActivity(app: MainActivity)

}