package com.akordirect.android_kotlin.dagger2

import com.akordirect.android_kotlin.service.DB
import com.akordirect.android_kotlin.service.Info
import dagger.Module
import dagger.Provides

@Module
class InfoModule {

    @Provides
    fun provideInfo(): Info {
        return Info()
    }

    @Provides
    fun provideDB(): DB {
        return DB()
    }

}