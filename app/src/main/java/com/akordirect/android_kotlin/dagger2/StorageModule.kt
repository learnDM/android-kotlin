package com.akordirect.android_kotlin.dagger2

import com.akordirect.android_kotlin.service.DatabaseHelper
import dagger.Module
import dagger.Provides

@Module
class StorageModule {

    @Provides
    fun provideDatabaseHelper(): DatabaseHelper{
        return DatabaseHelper()
    }

}