package com.akordirect.android_kotlin.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity // The namespace for the Android Jetpack libraries is androidx.
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import com.akordirect.android_kotlin.R
import com.akordirect.android_kotlin.dagger2.DaggerHelperComponent
import com.akordirect.android_kotlin.service.DB


import com.akordirect.android_kotlin.service.DatabaseHelper
import com.akordirect.android_kotlin.service.Info
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class MainActivity : AppCompatActivity() {



    private val TAG = MainActivity::class.java.simpleName
    @Inject lateinit var info: Info
    @Inject lateinit var db: DB

    @Inject lateinit var helper: DatabaseHelper


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


//        var magicBox: InfoComponent = DaggerMagicBox.create()
//        magicBox.injectMainActivity(this)
//        DaggerHelperComponent.create().inject(this)
        DaggerHelperComponent.create().injectMainActivity(this)



        var buttonTest: Button = findViewById(R.id.button_test)
        var buttonGoToFragmentLesson: Button = findViewById(R.id.button_goto_fragmentLesson)
        var buttonGoToConstraintLayout: Button = findViewById(R.id.button_go_to_constraintLayout)
        var buttonGoToBinding: Button = findViewById(R.id.button_go_to_binding)
        var textViewHello: TextView = findViewById(R.id.textView_hello)

        textViewHello.setText(info.text + " + " + db.text )

        var observable = Observable.just(1, 2, 3, 4)
        var single = Single.just(5)
        var flowable = Flowable.just(1, 2, 3, 4, 5)

        buttonTest.setOnClickListener {
            Log.i(TAG, "Click button_test")

            textViewHello.setText( "" + helper.summ(5,10) )

        }

        buttonGoToFragmentLesson.setOnClickListener {
            val intentGallery = Intent(this, DiceActivity::class.java)
            startActivity(intentGallery)
        }


        buttonGoToConstraintLayout.setOnClickListener{
            startActivity(Intent(this, ConstraintLayoutActivity::class.java))
        }
        buttonGoToBinding.setOnClickListener{
            startActivity(Intent(this, BindingActivity::class.java))
        }


        var dispose = dataSourse()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                buttonTest.setText("new int $it")
                textViewHello.setText("new int $it")
               Log.i(TAG, "new data $it")
            }, { }, { })


    }

    fun dataSourse(): Observable<Int> {

        return Observable.create() { subscriber ->
            for (i in 0..100) {
                Thread.sleep(30000)
                subscriber.onNext(i)
            }
        }

    }
}
